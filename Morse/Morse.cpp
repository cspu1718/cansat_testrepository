#include "Arduino.h"
#include "Morse.h"


Morse::Morse(int parameterPinNumber,
	unsigned long parameterDotDuration,
	unsigned long parameterDashDuration,
	unsigned long parameterWaitDuration,
	char *parameterText,
	int parameterTextLength) {

	pinNumber = parameterPinNumber;
	dotDuration = parameterDotDuration;
	dashDuration = parameterDashDuration;
	waitDuration = parameterWaitDuration;
	text = parameterText;
	textLength = parameterTextLength;

  /**
   * Initialization of the array of Sign to a known value
   */
	for (int i=0; i<100; i++) {
		signs[i] = Sign::null;
	}

	/**
   * Translation of the array of char to the array of Sign
   * Loop through the input array of char
   * Use the mapping 2D array
	 */
	int posInText = 0;
	int posInSigns = 0;
	while (posInText < textLength) {
		int posInMap = int(text[posInText]) - int('a');
		if (posInMap >= 0 && posInMap < 26) {
  		for (int j=0; j<5; j++){
  			if (map[posInMap][j] != Sign::null){
  				signs[posInSigns] = map[posInMap][j];
  				posInSigns ++;
  				signs[posInSigns] = Sign::sp;
  				posInSigns ++;
  			}
  		}
		}

		posInText ++;
	}
 
	signs[posInSigns-1] = Sign::lp;
 
	/**
  * The array looks like this for sos
	* signs = {
	*	 Sign::dot, Sign::sp,
	*	 Sign::dot, Sign::sp,
	*	 Sign::dot, Sign::sp,
	*	 Sign::dash, Sign::sp,
	*	 Sign::dash, Sign::sp,
	*	 Sign::dash, Sign::sp,
	*	 Sign::dot, Sign::sp,
	*	 Sign::dot, Sign::sp,
	*	 Sign::dot, Sign::lp
	* };
	*/

	pos = 0; /**< Initial position in the array of sign */
	pinState = PinState::low; /**< Initial pin state */
  pinMode(pinNumber, OUTPUT); /**< Set the pin as an output pin */

	ts = millis(); /**< Initialize the timer */
}

void Morse::run() {
  /**
   * For each possible Sign state we do something different
   */
	switch (signs[pos]){
		case Sign::dot: /**< for a dot */
			updatePin(PinState::high); /**< It's a dot => light on */

			if (millis()-ts > dotDuration){ /**< Time to go ahead */
				pos++;
				ts = millis();
			}
			break;
		case Sign::dash: /**< for a dash */
			updatePin(PinState::high); /**< It's a dash => light on */

			if (millis()-ts > dashDuration){ /**< Time to go ahead */
				pos++;
				ts = millis();
			}
			break;
		case Sign::sp: /**< for a short pause */
			updatePin(PinState::low); /**< It's a pause => light off */

			if (millis()-ts > dotDuration){ /**< Time to go ahead */
				pos++;
				ts = millis();
			}
			break;
		case Sign::lp: /**< for a long pause */
			updatePin(PinState::low); /**< It's a pause => light off */

			if (millis()-ts > waitDuration){ /**< Time to go ahead */
				pos = 0; /**< Repeat the meaasage */
				ts = millis();
			}
			break;
		case Sign::null:
			break;
	}
}

void Morse::updatePin(PinState newState) {
	if(pinState == PinState::low && newState == PinState::high){ /**< light is off and we want it on */
		digitalWrite(pinNumber, HIGH);
		pinState = PinState::high;
	}
	else if(pinState == PinState::high && newState == PinState::low) { /**< light is on and we want it off */
		digitalWrite(pinNumber, LOW);
		pinState = PinState::low;
	}
}
